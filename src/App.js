import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import styles from "./App.module.css";

import Login from "./components/Login";
import CashbackCasino from "./components/CashbackCasino";
import Header from "./components/Header";
import Footer from "./components/Footer";
import HomePage from "./components/HomePage";
import { useSelector } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Registration from "./components/Registration";

export default function App() {
  const authorized = useSelector((state) => state.authReducer.authorized);

  return (
    <div className={styles.mainContainer}>
      <Router>
        <Switch>
          <div className={styles.contentContainer}>
            <Header />
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route path="/authorization">
              <Login />
            </Route>

            <Route path="/casino">
              {!authorized ? (
                <Redirect to={"/authorization"} />
              ) : (
                <CashbackCasino />
              )}
            </Route>
            <Route path="/registration">
              {authorized ? <Redirect to={"/"} /> : <Registration />}
            </Route>
            <Footer />
          </div>
        </Switch>
      </Router>
      <ToastContainer
        position="top-right"
        autoClose={3500}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}
