import { useState, useEffect } from "react";
import { NavLink, Redirect, Route, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import styles from "./Authorisation.module.css";

import { login } from "../../store/actionCreators/authActions";
import CashbackCasino from "../CashbackCasino";
import Registration from "../Registration";

export default function Login() {
  const dispatch = useDispatch();
  const authorized = useSelector((state) => state.authReducer.authorized);
  const history = useHistory();
  const handleSubmit = async (e) => {
    e.preventDefault();
    await dispatch(login(email, password));
  };
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    if (authorized) {
      history.push("/");
    }
  }, [history, authorized]);
  return (
    <div className={styles.authContainer}>
      <form className={styles.authForm} action="">
        <h1 className={styles.authForm__h1}>Сыграй в казино</h1>
        <h3 className={styles.authForm__h3}>и не проиграй квартиру...</h3>
        <div className={styles.authForm__input__container}>
          <input
            className={styles.authForm__input}
            type="email"
            placeholder={"Ваша почта"}
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </div>
        <div className={styles.authForm__input__container}>
          <input
            className={styles.authForm__input}
            placeholder={"Пароль"}
            value={password}
            type="password"
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
        </div>
        <button className={styles.tinkoff__button} onClick={handleSubmit}>
          <h4 className={styles.tinkoff__button__h4}>Войти</h4>
        </button>
      </form>
      <NavLink to="/registration">
        <p>Зарегистрироваться</p>
      </NavLink>
    </div>
  );
}
