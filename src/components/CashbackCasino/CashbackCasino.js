import { useEffect } from "react";
import styles from "./CashbackCasino.module.css";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { getUserInfo } from "../../store/actionCreators/cashBackActions";

import ChosenCashbacksList from "../ChosenCashbacksList";
import CashbackCard from "./CashbackCard";

export default function CashbackCasino() {
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.cashBackReducer.loading);
  const cashBacks = useSelector((state) => state.cashBackReducer.cashBacks);

  useEffect(() => {
    dispatch(getUserInfo());
  }, []);

  return (
    <div className={styles.wrapper}>
      {loading ? (
        <div className={styles.loaderContainer}>
          <Loader type="ThreeDots" color="#00BFFF" height={200} width={200} />
        </div>
      ) : (
        <div className={styles.cashBacksContainer}>
          <div className={styles.cashBacks}>
            {cashBacks.map((cashback) => {
              return (
                <CashbackCard
                  cashback={cashback.cashback}
                  id={cashback.id}
                  type={cashback.type}
                  openedByUser={cashback.openedByUser}
                  key={cashback.id}
                />
              );
            })}
          </div>
          <ChosenCashbacksList />
        </div>
      )}
    </div>
  );
}
