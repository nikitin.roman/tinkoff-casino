import styles from "./CashbackCard.module.css";
import { sendId } from "../../../store/actionCreators/cashBackActions";
import CoinSVG from "../../svgComponents/CoinSVG";
import { useSpring, animated as a } from "react-spring";
import { useDispatch } from "react-redux";
import { useEffect, useRef, useState } from "react";
import TinkoffLogoSVG from "../../svgComponents/TinkoffLogoSVG";

export default function CashbackCard(props) {
  const dispatch = useDispatch();
  const [flipped, set] = useState(props.type !== "");
  const { transform, opacity } = useSpring({
    opacity: flipped ? 1 : 0,
    transform: `perspective(400px) rotateX(${flipped ? 180 : 0}deg)`,
    config: { mass: 25, tension: 250, friction: 100 },
  });

  const useIsMount = () => {
    const isMountRef = useRef(true);
    useEffect(() => {
      isMountRef.current = false;
    }, []);
    return isMountRef.current;
  };
  const isMount = useIsMount();

  useEffect(() => {
    if (props.openedByUser && !isMount) {
      set((state) => !state);
    }
  }, [props.openedByUser]);

  return (
    <div
      className={styles.cashbackCard__container}
      onClick={() => dispatch(sendId(props.id))}
    >
      <a.div
        className={styles.cashbackCard}
        style={{
          opacity: opacity.interpolate((o) => 1 - o),
          transform,
        }}
      >
        <TinkoffLogoSVG />
      </a.div>

      <a.div
        className={styles.cashbackCard__content}
        style={{
          display: flipped ? "flex" : "none",
          opacity,
          transform: transform.interpolate((t) => `${t} rotateX(180deg)`),
        }}
      >
        <CoinSVG />
        <p>{props.cashback}% кэшбэка на</p>
        <p>{props.type}</p>
      </a.div>
    </div>
  );
}
