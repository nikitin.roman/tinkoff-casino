import { NavLink } from "react-router-dom";
import styles from "./HomePage.module.css";

import { useSelector } from "react-redux";
import { dropBase } from "../../store/store";

export default function HomePage() {
  const authorized = useSelector((state) => state.authReducer.authorized);
  const login = useSelector((state) => state.authReducer.login);
  return (
    <div className={styles.contentContainerBody}>
      <div className={styles.contentContainerBody__leftPart}>
        <h1 className={styles.contentContainerBody__leftPart__h1}>
          А ты готов?!
        </h1>
        <p className={styles.contentContainerBody__leftPart__p}>
          проиграть в казино...
        </p>
        <NavLink to="/casino">
          <button className={styles.tinkoff__button}>
            <h4 className={styles.tinkoff__button__h4}>Играть!</h4>
          </button>
        </NavLink>
        {authorized ? (
          <button
            className={styles.tinkoff__button}
            onClick={() => dropBase(login)}
          >
            Сбросить базу
          </button>
        ) : (
          <div />
        )}
      </div>
      <div className={styles.contentContainerBody__rightPart} />
    </div>
  );
}
