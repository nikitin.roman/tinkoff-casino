import LoginSVG from "../svgComponents/LoginSVG.js";
import styles from "./Header.module.css";
import { NavLink, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../store/actionCreators/authActions";
import { logoutCB } from "../../store/actionCreators/cashBackActions";

export default function Header() {
  const authorized = useSelector((state) => state.authReducer.authorized);
  const history = useHistory();
  const dispatch = useDispatch();
  const logOut = () => {
    dispatch(logout());
    dispatch(logoutCB());
    history.push("/authorization");
  };
  return (
    <div className={styles.headerContainer}>
      <div
        onClick={() => history.push("/")}
        className={styles.headerContainer__tinkoffLogo}
      />
      <div>
        <h4 className={styles.headerContainer__header}>Кэшбэк казино</h4>
      </div>
      <div />
      <div />
      <div />
      <div />
      <div />

      {authorized === false ? (
        <NavLink to="/authorization">
          <button className={styles.headerContainer__button}>
            <p className={styles.headerContainer__button__text}>Войти</p>
            <LoginSVG />
          </button>
        </NavLink>
      ) : (
        <button className={styles.headerContainer__button} onClick={logOut}>
          <p className={styles.headerContainer__button__text}>Выйти</p>
          <LoginSVG />
        </button>
      )}
    </div>
  );
}
