import styles from "./Registration.module.css";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { register } from "../../store/actionCreators/authActions";
import { useEffect, useState } from "react";

export default function Registration() {
  const history = useHistory();
  const dispatch = useDispatch();
  const authorized = useSelector((state) => state.authReducer.authorized);
  const handleSubmit = async (e) => {
    e.preventDefault();
    await dispatch(register(email, password));
  };
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    if (authorized) {
      history.push("/");
    }
  }, [history, authorized]);
  return (
    <div className={styles.authContainer}>
      <form className={styles.authForm} action="">
        <h1 className={styles.authForm__h1}>Зарегистрируйся</h1>
        <h3 className={styles.authForm__h3}>
          и выиграй уже хоть что-нибудь...
        </h3>
        <div className={styles.authForm__input__container}>
          <input
            className={styles.authForm__input}
            type="email"
            placeholder={"Ваша почта"}
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </div>
        <div className={styles.authForm__input__container}>
          <input
            className={styles.authForm__input}
            placeholder={"Придумайте пароль"}
            value={password}
            type="password"
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
        </div>
        <button className={styles.tinkoff__button} onClick={handleSubmit}>
          <h4 className={styles.tinkoff__button__h4}>Зарегистрироваться</h4>
        </button>
      </form>
    </div>
  );
}
