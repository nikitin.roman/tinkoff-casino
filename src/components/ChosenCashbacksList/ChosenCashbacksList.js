import styles from "./ChosenCashbacksList.module.css";
import React from "react";
import { useSelector } from "react-redux";

export default function ChosenCashbacksList() {
  const cashBacksList = useSelector(
    (state) => state.cashBackReducer.cashBacksList
  );
  return (
    <div className={styles.cashBacks__list}>
      {cashBacksList.map((x) => {
        return (
          <div className={styles.cashBacks__list__item} key={x.type + x.id}>
            <p>{x.type}</p>
            <p>{x.cashback}%</p>
          </div>
        );
      })}
    </div>
  );
}
