import styles from "./Footer.module.css";

export default function Footer() {
  return (
    <div className={styles.mailContainer}>
      <a
        className={styles.mailContainer__link}
        href="mailto:nikitin.roman2000@yandex.ru?subject=Приходи к нам работать!"
      >
        Nikitin.roman2000@yandex.ru
      </a>
    </div>
  );
}
