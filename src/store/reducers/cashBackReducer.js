import { cashBackConstants } from "../constants/cashBackConstants";
import { TRYINGS } from "../constants/constants";

const initialState = {
  cashBacks: [],
  cashBacksList: [],
  cashBacksShadow: [],
  cashBackStatus: false,
  tryings: TRYINGS,
  loading: false,
  wins: 0,
};

export const cashBackReducer = (state = initialState, action) => {
  switch (action.type) {
    case cashBackConstants.SET_CASHBACK_STATUS:
      return { ...state, cashBackStatus: true };
    case cashBackConstants.APPEND_NEW_CASHBACK:
      return {
        ...state,
        cashBacksList: [...state.cashBacksList, action.payload],
        cashBacksShadow: [...state.cashBacksShadow, action.payload],
      };
    case cashBackConstants.CHANGE_TRYINGS:
      return { ...state, tryings: state.tryings - 1 };
    case cashBackConstants.GET_ALL_CASHBACKS:
      return {
        ...state,
        tryings: state.wins,
        cashBacks: action.payload.cashBacks,
      };
    case cashBackConstants.SET_OLD_CASHBACKS:
      return {
        ...state,
        cashBacks: action.payload.cashBacks,
        cashBacksList: action.payload.cashBacksList,
        cashBacksShadow: action.payload.cashBacksShadow,
      };
    case cashBackConstants.GET_EMPTY_CASHBACK_LIST:
      return { ...state, cashBacks: action.payload.cashBacks };
    case cashBackConstants.LOADING:
      return { ...state, loading: !state.loading };
    case cashBackConstants.INCREASE_WINS:
      return { ...state, wins: state.wins + 1 };
    case cashBackConstants.DECREASE_TRYINGS:
      return { ...state, tryings: state.tryings - 1 };
    case cashBackConstants.MAX_CASHBACK:
      return { ...state, cashBacksList: [...action.payload] };
    case cashBackConstants.CHECK_WIN:
      return { ...state, cashBacksList: [...action.payload] };
    case cashBackConstants.COMPARE_CASHBACKS:
      return { ...state, cashBacks: [...action.payload] };
    case cashBackConstants.LOGOUT:
      return {
        ...state,
        cashBackStatus: false,
        cashBacks: [],
        cashBacksList: [],
        cashBacksShadow: [],
        tryings: TRYINGS,
        loading: false,
        wins: 0,
      };
    case cashBackConstants.APPEND_MAX_CASHBACK:
      return {
        ...state,
        cashBacksList: [...state.cashBacksList, action.payload],
      };
    default:
      return state;
  }
};
