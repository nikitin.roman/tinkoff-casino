import { authConstants } from "../constants/authConstants";

const initialState = {
  login: "",
  password: "",
  authorized: false,
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case authConstants.LOGIN:
      return {
        ...state,
        login: action.payload.login,
        password: action.payload.password,
        authorized: true,
      };
    case authConstants.LOGOUT:
      return { ...state, login: "", password: "", authorized: false };
    case authConstants.LOGIN_FAILED:
      return { ...state, authorized: false };
    default:
      return state;
  }
};
