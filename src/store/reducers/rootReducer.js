import { authReducer } from "./authReducer";
import { cashBackReducer } from "./cashBackReducer";
import { combineReducers } from "redux";

const reducer = combineReducers({
  authReducer,
  cashBackReducer,
});

export default reducer;
