import { authConstants } from "../constants/authConstants";
import { loginFB, logoutFB, registerFB } from "../store";

/**
 * Функция для логина на сайте
 * @param login почта для входа
 * @param password пароль для входа
 * @returns {(function(*): Promise<void>)|*}
 */
export const login = (login, password) => async (dispatch) => {
  if (await loginFB(login, password)) {
    dispatch({
      type: authConstants.LOGIN,
      payload: { login, password },
    });
  } else {
    dispatch({
      type: authConstants.LOGIN_FAILED,
    });
  }
};
export const register = (login, password) => async (dispatch) => {
  if (await registerFB(login, password)) {
    dispatch({
      type: authConstants.LOGIN,
      payload: { login, password },
    });
  } else {
    dispatch({
      type: authConstants.LOGIN_FAILED,
    });
  }
};
/**
 * Функция для выхода из аккаунта
 * @returns {(function(*): void)|*}
 */
export const logout = () => (dispatch) => {
  logoutFB();
  dispatch({
    type: authConstants.LOGOUT,
  });
};
