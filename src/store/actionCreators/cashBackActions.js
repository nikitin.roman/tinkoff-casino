import { cashBackConstants } from "../constants/cashBackConstants";
import {
  getFullInfo,
  getIdListFB,
  getInfoById,
  sendToFB,
  getUserInfoFB,
} from "../store";

import { MAX_CASHBACK_EDITABLE, CASHBACK_TO_WIN } from "../constants/constants";
import { toast } from "react-toastify";

/**
 * Функция для добавления нового объекта кешбека в список
 * @param id номер кешбека
 * @returns {(function(*, *): Promise<void>)|*}
 */
export const appendNewCashBack = (id) => async (dispatch, getState) => {
  const cashBack = await getInfoById(id);

  cashBack["openedByUser"] = true;

  dispatch({
    type: cashBackConstants.APPEND_NEW_CASHBACK,
    payload: cashBack,
  });

  const cashBacks = [...getState().cashBackReducer.cashBacks];

  for (let i of cashBacks) {
    if (i.id === id) {
      cashBacks.find((x) => x.id === id).type = cashBack.type;
      cashBacks.find((x) => x.id === id).openedByUser = true;
    }
  }

  dispatch({
    type: cashBackConstants.COMPARE_CASHBACKS,
    payload: cashBacks,
  });
};

/**
 * Функция для установления статуса, что кешбек получен
 * @param login логин пользователя
 * @returns {(function(*): void)|*}
 */
export const setCashBackStatus = (login) => (dispatch) => {
  dispatch({
    type: cashBackConstants.SET_CASHBACK_STATUS,
    payload: login,
  });
};

/**
 * Функция для изменения количества оставшихся попыток. В случае тыка на уже выбранный ранее кешбек, попытка не сгорает.
 * @returns {(function(*, *): void)|*}
 */
export const changeTryings = () => (dispatch, getState) => {
  const cashBacksList = getState().cashBackReducer.cashBacksList;

  if (cashBacksList.length > 1) {
    for (let i = 0; i < cashBacksList.length - 1; i++) {
      if (
        cashBacksList[i].type === cashBacksList[cashBacksList.length - 1].type
      ) {
        console.log("попытка не сгорела");
        return;
      }
    }
  }

  dispatch({
    type: cashBackConstants.CHANGE_TRYINGS,
  });
};

/**
 * Функция для получения всех кешбеков разом, вызывается в конце игры
 * @returns {(function(*): Promise<void>)|*}
 */
export const getAllCashBacks = () => async (dispatch) => {
  const cashBacks = await getFullInfo();

  for (let i of cashBacks) {
    i["openedByUser"] = true;
  }

  dispatch({
    type: cashBackConstants.GET_ALL_CASHBACKS,
    payload: { cashBacks: [...cashBacks] },
  });
};
/**
 * Функция для изменения всех полученных кешбеков на 10-процентные. В случае выигрыша (2 кешбека выбито по 3 раза)
 * @returns {(function(*, *): void)|*}
 */
export const maxCashback = () => (dispatch, getState) => {
  const lst = [];
  const cashBacksList = getState().cashBackReducer.cashBacksList;

  cashBacksList.map((cashBack) =>
    lst.push({ cashback: MAX_CASHBACK_EDITABLE, type: cashBack.type })
  );

  dispatch({
    type: cashBackConstants.MAX_CASHBACK,
    payload: lst,
  });

  sendToFB(getState().authReducer.login, {
    wins: 2,
    cashBackEarned: true,
    cashBacksList: getState().cashBackReducer.cashBacksList,
    cashBacksShadow: getState().cashBackReducer.cashBacksShadow,
  });
};

/**
 * Функция для отображения ранее выбранных кешбеков, лежащих в базе
 * @param cashBacks кешбеки из карточек
 * @param cashBacksList кешбеки из списка справа
 * @param cashBacksShadow список кешбеков для сопоставления карточек кешбеков
 * @returns {(function(*): void)|*}
 */
export const setOldCashbacks = (cashBacks, cashBacksList, cashBacksShadow) => (
  dispatch
) => {
  dispatch({
    type: cashBackConstants.SET_OLD_CASHBACKS,
    payload: {
      cashBacks: cashBacks,
      cashBacksList: cashBacksList,
      cashBacksShadow: cashBacksShadow,
    },
  });
};

/**
 * Функция для получения информации о залогиненном пользователе. Тут же и логика продолжения прерванной игры
 * @returns {(function(*, *): Promise<void>)|*}
 */
export const getUserInfo = () => async (dispatch, getState) => {
  const cashBacks = getState().cashBackReducer.cashBacks;
  if (cashBacks.length > 0) {
    return;
  }

  dispatch({
    type: cashBackConstants.LOADING,
  });
  const data = await getUserInfoFB(getState().authReducer.login);

  if (
    data.cashBackEarned &&
    data.cashBacksList.length >= 3 &&
    data.wins !== 1
  ) {
    toast("В этом месяце вы уже забрали свой кешбек!");
    dispatch(
      setOldCashbacks(
        data.cashBacksList,
        data.cashBacksList,
        data.cashBacksShadow
      )
    );
  } else if (data.wins === 1) {
    dispatch(increaseWins());
    dispatch(setOldCashbacks([], data.cashBacksList, data.cashBacksShadow));
    dispatch(getAllCashBacks());
    toast("Вы уже выбрали несколько кешбеков ранее, мы их отобразили.");
    toast(
      "Поздравляем, вы выиграли " +
        data.wins +
        " суперприз/а! Вы можете выбрать любой кэшбэк!"
    );
  } else if (data.cashBackEarned && data.cashBacksList.length <= 3) {
    toast("Вы уже выбрали несколько кешбеков ранее, мы их отобразили.");
    dispatch(getEmptyCashbackList());

    const cashBacks = await getFullInfo();
    for (let i of cashBacks) {
      for (let j of data.cashBacksShadow) {
        if (j.id === i.id) {
          i["openedByUser"] = true;
          i.cashback = j.cashback;
        }
      }
      if (!i.openedByUser) {
        i.type = "";
      }
    }

    dispatch(
      setOldCashbacks(cashBacks, data.cashBacksList, data.cashBacksShadow)
    );
    for (let i of data.cashBacksList) {
      dispatch(decreaseTryings());
    }
  } else {
    dispatch(getEmptyCashbackList());
  }
  dispatch({
    type: cashBackConstants.LOADING,
  });
};
/**
 * Функция для получения пустого списка кешбеков. Вызывается в начале игры для отрисовки пустых карточек кешбеков
 * @returns {(function(*, *): Promise<void>)|*}
 */
export const getEmptyCashbackList = () => async (dispatch, getState) => {
  const cashBacks = getState().cashBackReducer.cashBacks;

  if (cashBacks.length > 0) {
    return;
  }
  const idList = await getIdListFB();
  for (let i of idList.id) {
    cashBacks.push({ id: i, type: "", cashback: 5, openedByUser: false });
  }

  dispatch({
    type: cashBackConstants.GET_EMPTY_CASHBACK_LIST,
    payload: { cashBacks: [...cashBacks] },
  });
};

/**
 * Функция для увеличения количества выигрышей (от этого зависит дополнительное количество попыток выбора кешбека
 * @returns {(function(*): Promise<void>)|*}
 */
export const increaseWins = () => (dispatch) => {
  dispatch({
    type: cashBackConstants.INCREASE_WINS,
  });
};

/**
 * Функция для уменьшения количества выигрышей.
 * @returns {(function(*, *): void)|*}
 */
export const decreaseTryings = () => (dispatch, getState) => {
  if (getState().cashBackReducer.tryings > 0) {
    dispatch({
      type: cashBackConstants.DECREASE_TRYINGS,
    });
  } else toast("Попытки закончились!");
};
/**
 * Функция для проверки списка кешбеков на наличие в нем выбранного кешбека. Если такой кешбек есть, в список он не добавляется, а на уже выбитый кешбек +1 процент.
 * @returns {(function(*, *): void)|*}
 */
export const checkWin = () => (dispatch, getState) => {
  const cashBacksList = getState().cashBackReducer.cashBacksList;
  const object = {};
  const lst = [];

  for (let i of cashBacksList) {
    if (object[i.type]) {
      object[i.type].cashback += 1;
    } else {
      object[i.type] = {
        id: i.id,
        cashback: i.cashback,
        type: i.type,
        openedByUser: true,
      };
    }
  }
  for (let i in object) {
    lst.push(object[i]);
  }

  dispatch({
    type: cashBackConstants.CHECK_WIN,
    payload: lst,
  });

  sendToFB(getState().authReducer.login, {
    wins: 0,
    cashBackEarned: true,
    cashBacksList: lst,
    cashBacksShadow: getState().cashBackReducer.cashBacksShadow,
  });
};

/**
 * Функция окончания игры. Считает количество выигрышей по кешбекам, настаканным в списке кешбеков.
 * @returns {(function(*, *): Promise<void>)|*}
 */
export const endOfGame = () => async (dispatch, getState) => {
  const cashBacksList = getState().cashBackReducer.cashBacksList;
  let wins = getState().cashBackReducer.wins;

  for (let i of cashBacksList) {
    if (i.cashback === CASHBACK_TO_WIN) {
      dispatch(increaseWins());
      wins = getState().cashBackReducer.wins;
    }
  }
  if (wins === 2) {
    dispatch(maxCashback());
    toast("Поздравляем, вы выиграли все максимальные кэшбеки!!!");
  }
  if (wins === 1) {
    toast(
      "Поздравляем, вы выиграли " +
        wins +
        " суперприз/а! Вы можете выбрать любой кэшбэк!"
    );
    sendToFB(getState().authReducer.login, {
      wins: 1,
      cashBackEarned: true,
      cashBacksList: getState().cashBackReducer.cashBacksList,
      cashBacksShadow: getState().cashBackReducer.cashBacksShadow,
    });
  }
  await dispatch(getAllCashBacks());

  dispatch({
    type: cashBackConstants.END_OF_GAME,
  });
};

/**
 * Функция для добавления выигранного кешбека. Он будет с максимальным процентом.
 * @returns {(function(*): void)|*}
 * @param cashback кешбек (объект)
 */
export const appendMaxCashBack = (cashback) => (dispatch) => {
  const newCashback = { ...cashback, cashback: MAX_CASHBACK_EDITABLE };

  dispatch({
    type: cashBackConstants.APPEND_MAX_CASHBACK,
    payload: newCashback,
  });
  dispatch({
    type: cashBackConstants.CHANGE_TRYINGS,
  });
};

/**
 * Функция для выбора последнего кешбека (выигранного)
 * @param cashback объект кешбека
 * @returns {(function(*, *): void)|*}
 */
export const choosePrise = (cashback) => (dispatch, getState) => {
  const tryings = getState().cashBackReducer.tryings;
  const cashBacksList = getState().cashBackReducer.cashBacksList;

  if (tryings === 0) {
    toast("Вы уже выбрали все доступные призы!");
    return;
  }
  let uniqueFlag = 0;
  for (let i of cashBacksList) {
    if (i.type === cashback.type) {
      uniqueFlag += 1;
    }
  }
  if (uniqueFlag > 0) {
    toast("Выберите новый кэшбек!");
    return;
  }

  dispatch(appendMaxCashBack(cashback));

  sendToFB(getState().authReducer.login, {
    wins: 0,
    cashBackEarned: true,
    cashBacksList: getState().cashBackReducer.cashBacksList,
    cashBacksShadow: getState().cashBackReducer.cashBacksShadow,
  });
};
export const logoutCB = () => (dispatch) => {
  dispatch({
    type: cashBackConstants.LOGOUT,
  });
};
/**
 * Функция, вызываемая по клику на карточку кешбека. В ней живет основная логика всей игры.
 * @param id айдишник кешбека
 * @returns {(function(*, *): Promise<void>)|*}
 */
export const sendId = (id) => async (dispatch, getState) => {
  const wins = getState().cashBackReducer.wins;
  const cashBacks = getState().cashBackReducer.cashBacks;

  if (wins === 1) {
    dispatch(choosePrise(cashBacks.find((x) => x.id === id)));
    return;
  }
  if (cashBacks.find((x) => x.id === id).type !== "") {
    toast("Выберите другой кэшбэк");
    return;
  }
  const tryings = getState().cashBackReducer.tryings;
  const cashBackStatus = getState().cashBackReducer.cashBackStatus;

  if (tryings > 0) {
    if (!cashBackStatus) {
      dispatch(setCashBackStatus());
    }
    await dispatch(appendNewCashBack(id));
    await dispatch(changeTryings());
    await dispatch(checkWin());
    return;
  }
  toast("Попытки закончились! Наслаждайтесь Вашим кешбеком!");
  await dispatch(await endOfGame());
};
