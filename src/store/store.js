import firebase from "firebase";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import reducer from "./reducers/rootReducer";
import { toast } from "react-toastify";

const firebaseConfig = {
  apiKey: "AIzaSyBJZY8GAg-mkSZUkt89xk16t1qqnSyhbuo",
  authDomain: "tinkoff-casino.firebaseapp.com",
  projectId: "tinkoff-casino",
  storageBucket: "tinkoff-casino.appspot.com",
  messagingSenderId: "500927324941",
  appId: "1:500927324941:web:d90320727a6565b97f4644",
  measurementId: "G-793C9SE7WE",
};
firebase.initializeApp(firebaseConfig);
let db = firebase.firestore();

/**
 * Функция для логина на сайте
 * @param email почта для входа
 * @param password пароль для входа
 * @returns {Promise<T|boolean>} промис, при разрешении которого возвращается булев тип, зашли ли мы на сайт
 */
export const loginFB = async (email, password) => {
  return await firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then((userCredential) => {
      let user = userCredential.user;
      console.log(user);
      console.log(user.email);
      return true;
    })
    .catch((error) => {
      toast("" + error.code + error.message);
      console.log("login error", error.message);
      return false;
    });
};
export const registerFB = async (email, password) => {
  return await firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then((userCredential) => {
      let user = userCredential.user;
      db.collection("users").doc(email).set(
        {
          cashBackEarned: false,
        },
        { merge: true }
      );
      return true;
    })
    .catch((error) => {
      toast("" + error.code + error.message);
      console.log("register error", error.message);
      return false;
    });
};
export const sendToFB = (email, data) => {
  db.collection("users")
    .doc(email)
    .set(data)
    .then(() => {
      console.log("Document successfully written!");
    })
    .catch((error) => {
      console.error("Error writing document: ", error);
    });
};

/**
 * Функция для получения информации о залогиненном пользователе из базы
 * @param login логин пользователя
 * @returns {Promise<T>}
 */
export const getUserInfoFB = async (login) => {
  const docRef = db.collection("users").doc(login);

  return await docRef
    .get()
    .then((doc) => {
      if (doc.exists) {
        console.log("Document data:", doc.data());
        return doc.data();
      } else {
        console.log("No such document!");
      }
    })
    .catch((error) => {
      console.log("Error getting document:", error);
    });
};

/**
 * Функция, которая ходит в базу и достает оттуда список кешбеков без наименования кешбека. Для первичного заполнения карточек.
 * @returns {Promise<T|boolean>} возвращает список
 */
export const getIdListFB = async () => {
  let listRef = db.collection("cashBacksList").doc("list");
  return await listRef
    .get()
    .then((doc) => {
      if (doc.exists) {
        return doc.data();
      } else {
        console.log("No such document!");
        return false;
      }
    })
    .catch((error) => {
      console.log("Error getting document:", error);
      return false;
    });
};
/**
 * Функция для выхода из аккаунта
 */
export const logoutFB = () => {
  firebase
    .auth()
    .signOut()
    .then(() => {
      console.log("Вышел");
    })
    .catch((error) => {
      console.log(error);
    });
};
/**
 * Достает из базы информацию о кешбеке, выбранном пользователем. Тягаем каждый по отдельности, чтобы низя было в нетворке поглядеть все кешбеки разом.
 * @returns {Promise<T|boolean>} возвращает объект с информацией о кешбеке
 */
export const getInfoById = async (id) => {
  let cashBacksRef = db.collection("cashBacks").doc("" + id);

  return await cashBacksRef
    .get()
    .then((doc) => {
      if (doc.exists) {
        return doc.data();
      } else {
        console.log("No such document!");
        return false;
      }
    })
    .catch((error) => {
      console.log("Error getting document:", error);
      return false;
    });
};

/**
 * Идет в базу за всеми кешбеками разом. Вызывается в конце игры.
 * @returns {Promise<T|boolean>} возвращает список объектов всех кешбеков
 */
export const getFullInfo = async () => {
  let cashBacksList = [];
  return await db
    .collection("cashBacks")
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        cashBacksList = [...cashBacksList, doc.data()];
      });
      return cashBacksList;
    })
    .catch((error) => {
      console.log("Error getting document:", error);
      return false;
    });
};
/**
 * Вспомогательная функция для сброса базы, чтобы проверяющему не пришлось бегать в базу и прввить ручками.
 * @param email - логин пользователя
 */
export const dropBase = (email) => {
  db.collection("users")
    .doc(email)
    .set({ cashBackEarned: false })
    .then(() => {
      toast("Document successfully written (dropped)!");
    })
    .catch((error) => {
      console.error("Error writing document: ", error);
    });
};

const middleware = [thunk];
const store = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
